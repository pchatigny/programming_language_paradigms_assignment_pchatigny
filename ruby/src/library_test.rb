# Programming Paradigms 2014-15 Assignment
# Object-Oriented Programming with Ruby
#
# Author::    Patrick Chatigny
# Date::      1 February 2015
# 
# Test file for library.rb 


$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require 'library'

class Library_test < Test::Unit::TestCase

  
#############################
#                           #
#       Calendar Tests      #
#                           #
#############################

  
  def test_get_date() 
    
    date = Calendar.new
    
    # Test day 0 - the default state
    assert_equal(0, date.get_date)
    
    # Test day 5 - advance 5 times
    5.times { date.advance }
    assert_equal(5, date.get_date)
    
  end 
  
  
  def test_advance() 
    
    date = Calendar.new
    
    # Advance 5 days which should make day 5
    5.times { date.advance }
    assert_equal(5, date.get_date)
    
    # Advance 55 days which should make day 60
    60.times { date.advance }
    assert_equal(65, date.get_date)

    # Advance 100 days which should make day 165
    100.times { date.advance }
    assert_equal(165, date.get_date)
    
  end   

  
#############################
#                           #
#         Book Tests        #
#                           #
#############################  

  
  def test_get_id()
    
    # Should return the book's unique identification number.
    book = Book.new(1234, "My Book", "Pat Chatigny")
    assert_equal(1234, book.get_id)
    
    # Should return the book's unique identification number.
    book = Book.new(5678, "My Book", "Pat Chatigny")
    assert_equal(5678, book.get_id)
    
  end
  
  
  def test_get_title()
    
    # Should return this book's title.
    book = Book.new(1234, "My Book", "Pat Chatigny")
    assert_equal("My Book", book.get_title)

    # Should return this book's title.
    book = Book.new(1111, "Your Book", "Pat Chatigny")
    assert_equal("Your Book", book.get_title)
    
  end
  
  
  def test_get_author()
    
    # Should return this book's author.
    book = Book.new(1234, "My Book", "Pat Chatigny")
    assert_equal("Pat Chatigny", book.get_author)
    
    # Should return this book's author.
    book = Book.new(6789, "My Book", "New Author")
    assert_equal("New Author", book.get_author)

  end
  
  
  def test_get_due_date()
    
    book = Book.new(1234, "My Book", "Pat Chatigny")
    
    # Check default checkout date, which should be available (nil)
    assert_equal(nil, book.get_due_date)
    
    # Set a new due date and check date
    book.check_out(5)
    assert_equal(5, book.get_due_date)
    
    # Set a new due date and check date
    book.check_out(109)
    assert_equal(109, book.get_due_date)
    
  end
  
  
  def test_check_out()
    
    # Checkout book - should set the book's due date (as an integer).
    book = Book.new(1234, "My Book", "Pat Chatigny")
    book.check_out(5)
    assert_equal(5, book.get_due_date)
    
    # Checkout book - should set the book's due date (as an integer).
    book = Book.new(5678, "Your Book", "Pat Chatigny")
    book.check_out(15)
    assert_equal(15, book.get_due_date)

  end
  
  
  def test_check_in()
    
    book = Book.new(1234, "My Book", "Pat Chatigny")
    
    # Checkout, and then checkin book - should set the book's due date to nil.
    book.check_out(5)
    book.check_in()
    assert_equal(nil, book.get_due_date)
    
  end
  
  
  def test_to_s()

    # Should returns a string in the form of "id: title, by author"
    book = Book.new(1234, "My Book", "Pat Chatigny")   
    assert_equal("1234: My Book, by Pat Chatigny", book.to_s)
    
  end  
  

#############################
#                           #
#       Memebr Tests        #
#                           #
#############################  
  

  def test_get_name()
    
    #  Should return this member's name.
    member = Member.new("Patrick", "Library")
    assert_equal("Patrick", member.get_name)
    
  end

  
  def test_check_out()
    
    # Check that no books are checked out.
    member = Member.new("Patrick", "Library")
    assert_equal(0, member.get_books.length)
    
    # Checkout one book and test that it has been added to the set of books checked out by the member.
    b1 = Book.new(1111, "Book One", "Pat Chatigny")
    member.check_out(b1)
    assert_equal(1, member.get_books.length)
    assert_equal(true, member.get_books.any?{ |book| book == b1 })

    # Checkout two books and test that it has been added to the set of books checked out by the member.
    b2 = Book.new(2222, "Book Two", "Pat Chatigny")
    member.check_out(b2)
    assert_equal(2, member.get_books.length)
    assert_equal(true, member.get_books.any?{ |book| book == b2 })

    # Checkout three books and test that it has been added to the set of books checked out by the member.
    b3 = Book.new(3333, "Book Three", "Pat Chatigny")
    member.check_out(b3)
    assert_equal(3, member.get_books.length)
    assert_equal(true, member.get_books.any?{ |book| book == b3 })

    # Try to checkout a fourth book & make sure it is not there - (max of 3 books).
    b4 = Book.new(4444, "Book Four", "Pat Chatigny")
    member.check_out(b4)
    assert_equal(3, member.get_books.length)  
    assert_equal(false, member.get_books.any?{ |book| book == b4 })
    
  end


  def test_give_back()
    
    # Checkout three books
    member = Member.new("Patrick", "Library")
    b1 = Book.new(1111, "Book One", "Pat Chatigny")
    b2 = Book.new(2222, "Book Two", "Pat Chatigny")
    b3 = Book.new(3333, "Book Three", "Pat Chatigny")
    member.check_out(b1)
    member.check_out(b2)
    member.check_out(b3)
    assert_equal(3, member.get_books.length)
    
    # Remove a book and test that book is removed from the set of books checked out by the member. 
    member.give_back(b1)
    assert_equal(false, member.get_books.any?{ |book| book == b1 })

    # Remove a book and test that book is removed from the set of books checked out by the member. 
    member.give_back(b2)
    assert_equal(false, member.get_books.any?{ |book| book == b2 })

    # Remove a book and test that book is removed from the set of books checked out by the member. 
    member.give_back(b3)
    assert_equal(false, member.get_books.any?{ |book| book == b3 })    
  
  end

  
  def test_get_books()
    
    member = Member.new("Patrick", "Library")
    b1 = Book.new(1111, "Book One", "Pat Chatigny")
    b2 = Book.new(2222, "Book Two", "Pat Chatigny")
    b3 = Book.new(3333, "Book Three", "Pat Chatigny")
    
    # Should return the set of books containing the checked out book.
    member.check_out(b1)
    assert_equal(true, member.get_books.any?{ |book| book == b1 }) 
    
    # Should return the set of books containing the checked out book.
    member.check_out(b2)
    assert_equal(true, member.get_books.any?{ |book| book == b2 }) 
    
    # Should return the set of books containing the checked out book.
    member.check_out(b3)
    assert_equal(true, member.get_books.any?{ |book| book == b3 })  
    
    # Should return the set of book objects checked out to the member.
    assert_equal(3, member.get_books.length)

  end
  

  def test_send_overdue_notice()
    
    # Should print out this member's name along with the notice.
    member = Member.new("Patrick", "Library")  
    assert_equal("Patrick, you have overdue books.",  member.send_overdue_notice(", you have overdue books."))
    
  end
    
  
#############################
#                           #
#      Library Tests        #
#                           #
#############################    

  
  def test_get_current_member()
    
    # Test the default state - should be nil
    library = Library.new
    library.open
    assert_nil(library.get_current_member)

    # Add member and should return current member.
    library.issue_card("Patrick")
    library.serve("Patrick")
    assert_equal("Patrick", library.get_current_member.get_name)
    
  end
  

  def test_get_books()
    
    # Check that the available books match the default number of books.
    library = Library.new
    library.open    
    assert_equal(10, library.get_books.length)   
    
  end
  

  def test_create_books()
    
    # Check that the number of created books from a collection.txt file matches.
    library = Library.new
    library.open    
    assert_equal(10, library.get_books.length)  
  
  end

    
  def test_open()
    
    # Open library day 1.
    library = Library.new
    assert_equal("Today is day 1.", library.open)
    
    # Test day 23
    21.times { 
      library.close 
      library.open  
    }
    library.close # closing day 22
    assert_equal("Today is day 23.", library.open)
    
    # Try to open library when already opened - testing exception.
    assert_raise Exception do
      library.open
    end
    
  end
  
  
  def test_find_all_overdue_books()
    
    # Add members and create books.
    library = Library.new    
    library.open
    library.issue_card("Patrick")
    library.serve("Patrick")
    library.check_out(1,2) 
    assert_equal("None", library.find_overdue_books)
    library.issue_card("Keith")
    library.serve("Keith")
    library.check_out(3,4) 
    assert_equal("None", library.find_overdue_books)
    library.issue_card("Jasmine")
    library.serve("Jasmine")
    library.check_out(5) 
    assert_equal("None", library.find_overdue_books)

    # Test that no books should be overdue.
    assert_equal("No books are overdue.", library.find_all_overdue_books)
    
    # Pass 100 days to create overdue books.
    100.times{
      library.close
      library.open      
    }

    # Renew Jasmine's book.
    library.renew(5)
    
    # Now test for overdue books - should be all except Jasmine's.
    assert_equal("Patrick's overdue books:\n----> title1\n----> title2\nKeith's overdue books:\n----> title3\n----> title4\n", library.find_all_overdue_books)
    
  end
  
  
  def test_issue_card()
    
    # Test when library is closed.
    library = Library.new
    assert_raise Exception do
      library.issue_card("Patrick")
    end    
    
    # Test adding new member.
    library.open
    assert_equal("Library card issued to Patrick.", library.issue_card("Patrick"))

    # Testing that a member can have no more than one card - try to add an existing member.
    assert_equal("Patrick already has a library card.", library.issue_card("Patrick"))
    
  end
  
  
  def test_serve()
    
    # Test when library is closed.
    library = Library.new
    assert_raise Exception do
      library.serve("Patrick")
    end 
    
    # Serve member that does not exist.
    library.open
    assert_equal("Patrick does not have a library card.", library.serve("Patrick"))

    # Try to serve member.
    library.issue_card("Patrick")
    assert_equal("Now serving Patrick.", library.serve("Patrick"))   
    
  end
  
  
  def test_find_overdue_books()
    
    # Try when library not open.
    library = Library.new    
    assert_raise Exception do
      library.find_overdue_books
    end       
    
    # Try when no member is being served.
    library.open
    assert_raise Exception do
      library.find_overdue_books
    end   

    # Check out books, and check that none are overdue.
    library.issue_card("Patrick")
    library.serve("Patrick")
    library.check_out(1,2,3) 
    assert_equal("None", library.find_overdue_books)

    # Pass 100 days and check for overdue books.
    100.times{
      library.close
      library.open      
    }
    assert_equal("1: title1, by author1\n2: title2, by author2\n3: title3, by author3\n", library.find_overdue_books)
    
  end
  
  
  def test_check_in()

    # Try when library not open.
    library = Library.new    
    assert_raise Exception do
      library.check_in(1)
    end       
    
    # Try when no member is being served.
    library.open
    assert_raise Exception do
      library.check_in(1)
    end   

    # Try checking-in a book that the member does not have.
    library.issue_card("Patrick")
    library.serve("Patrick")
    assert_raise Exception do
      library.check_in(1)
    end   

    # Checkout books and check they don't exist in available books.
    library.check_out(1,2,3)
    assert_equal(3, library.get_current_member.get_books.length)
    assert_equal(false, library.get_books.any? { |b| b.get_id == 1 })
    assert_equal(false, library.get_books.any? { |b| b.get_id == 2 })
    assert_equal(false, library.get_books.any? { |b| b.get_id == 3 })    
    
    # try to check in books.
    assert_equal("Patrick has returned 2 books.", library.check_in(1,2))   
    assert_equal(1, library.get_current_member.get_books.length)
    assert_equal("Patrick has returned 1 books.", library.check_in(3))
    assert_equal(0, library.get_current_member.get_books.length)
    
    # Have they been added to the collection of available books.
    assert_equal(true, library.get_books.any? { |b| b.get_id == 1 })
    assert_equal(true, library.get_books.any? { |b| b.get_id == 2 })
    assert_equal(true, library.get_books.any? { |b| b.get_id == 3 })

    # Have the due dates been set to nil?
    assert_equal(true, library.get_books.all? { |b| b.get_due_date.nil? } )  

  end

  
  def test_search()
    
    # Try a failed search.
    library = Library.new
    assert_equal("No books found.", library.search("this will find no book"))
    
    # Try to search with less than 4 chars
    assert_equal("Search string must contain at least four characters.", library.search("123"))    

    # Try to search with 4 or more chars
    assert_equal("6: Book One, by Pat Chatigny\n", library.search("Book One"))   

    # Test find author and/or title.
    assert_equal("6: Book One, by Pat Chatigny\n7: Book Two, by Pat Chatigny\n8: Book Three, by Pat Chatigny\n9: Duplicate Book, by Duplicate Author\n", library.search("Book"))        
    assert_equal("1: title1, by author1\n2: title2, by author2\n3: title3, by author3\n4: title4, by author4\n5: title5, by author5\n", library.search("title"))        
    
    # Test case insensitivity.
    assert_equal("6: Book One, by Pat Chatigny\n", library.search("BOOK one"))            
    
    # Test multi-line results and to_s.
    assert_equal("1: title1, by author1\n2: title2, by author2\n3: title3, by author3\n4: title4, by author4\n5: title5, by author5\n", library.search("title"))
    
    # Test duplicate copies should return one copy only.
    assert_equal("9: Duplicate Book, by Duplicate Author\n", library.search("duplicate"))
    
  end
  
  
  def test_check_out()
    
    # Try to checkout when library not open.
    library = Library.new    
    assert_raise Exception do
      library.check_out(1)
    end       
    
    # Try to checkout when no member is being served.
    library.open
    assert_raise Exception do
      library.check_out(1)
    end   

    # Try to checkout a book_id that does not exist.
    assert_raise Exception do
      library.check_out(1000)
    end      
    
    # Try checking out books when more than three are checked out by one member.
    library.issue_card("Patrick")
    library.serve("Patrick")
    assert_raise Exception do
      library.check_out(1,2,3,4)
    end   
    
    # Check that available books are not checked out (due date is nil) and exist in collection
    assert_equal(true, library.get_books.all? { |b| b.get_due_date.nil? } )  
    assert_equal(true, library.get_books.any? { |b| b.get_id == 1 })
    assert_equal(true, library.get_books.any? { |b| b.get_id == 2 })
    assert_equal(true, library.get_books.any? { |b| b.get_id == 3 })
    
    # Checkout books three books and check if they have been added to member.
    assert_equal("2 books have been checked out to Patrick.", library.check_out(1,2))   
    assert_equal("1 books have been checked out to Patrick.", library.check_out(3))
    assert_equal(3, library.get_current_member.get_books.length)
    
    # Check that the books have been checked out.
    assert_equal(false, library.get_current_member.get_books.all? { |b| b.get_due_date.nil? } )
    
    # Check that the books have been removed from collection
    assert_equal(false, library.get_books.any? { |b| b.get_id == 1 })
    assert_equal(false, library.get_books.any? { |b| b.get_id == 2 })
    assert_equal(false, library.get_books.any? { |b| b.get_id == 3 })
    
  end
  
  
  def test_renew()
    
    # Try when library not open.
    library = Library.new    
    assert_raise Exception do
      library.renew(1)
    end       
    
    # Try when no member is being served.
    library.open
    assert_raise Exception do
      library.renew(1)
    end       
  
    # Try when no book ID is not checked-out to member.
    library.issue_card("Patrick")
    library.serve("Patrick")
    assert_raise Exception do
      library.renew(1)
    end   
    
    # Checkout books.
    library.check_out(1)
    library.check_out(2,3)
    assert_equal(true, library.get_current_member.get_books.all? { |b| b.get_due_date == 8 } )  # original due date Day 1 + 7 = due day 8
    
    # Advance 5 days and try to renew books.
    5.times{
      library.close
      library.open      
    }
    library.renew(1)
    library.renew(2,3)
    assert_equal(true, library.get_current_member.get_books.all? { |b| b.get_due_date == 13 } )  # Current date 1 + 5 and then new renew + 7 = new due day 13
    
    # Test messages.
    assert_equal("3 books have been renewed for Patrick.", library.renew(1,2,3)) 
  
  end

 
  def test_close()
   
    # Try to close library.
    library = Library.new
    library.open
    assert_equal("Good night.", library.close)
    
    # Try to close library when already closed - testing exception.
    assert_raise Exception do
      library.close
    end    
    
  end
  
  
  def test_quit()

    # Try to quit - should return the string "The library is now closed for renovations.".
    library = Library.new
    assert_equal("The library is now closed for renovations.",  library.quit)
   
  end

end