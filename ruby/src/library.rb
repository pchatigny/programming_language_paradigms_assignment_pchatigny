# Programming Paradigms 2014-15 Assignment
# Object-Oriented Programming with Ruby
#
# Author::    Patrick Chatigny
# Date::      1 February 2015
# 
# This program implements software for a local library as required by the assignment. 
  

# The Calendar class Keeps track of how many days have passed as an integer.
# Only one Calendar object is to be instantiated.
class Calendar
  
  @date 
  
  
  # The constructor which sets the date to zero. 
  def initialize
    @date = 0
  end
  
  
  # Returns the current date as an integer.
  def get_date()
    @date
  end
  
  
  # Increment the day to the next day and return the new date.
  def advance()
    @date = @date + 1
  end
  
end


# A class that represents a book.
class Book
  
  @id
  @title
  @author
  @due_date
  
  
  # The constructor sets the properties and sets the book to not checked out.
  # Params:
  # +id+:: the unique book id.
  # +title+:: the book's title (one author per book).
  # +author+:: the book's author.

  def initialize(id, title, author)
    
    @id = id
    @title = title
    @author = author 
    @due_date = nil   # The due date is nil if the book is not checked out. 
    
  end
  
  
  # Returns this book's unique identification number.
  def get_id()
    @id
  end
  
  
  # Returns this book's title.
  def get_title()
    @title
  end
  
  
  # Returns this book's author.
  def get_author()
    @author
  end
  
  
  # Returns the book's due date (as an integer).
  def get_due_date()
    @due_date
  end
  
  
  # Sets the book's due date.
  # Params:
  # +due_date+:: The book's due date.
  def check_out(due_date)
    @due_date = due_date
  end
  
  
  # Sets the book's due date to nil.
  def check_in()
    @due_date = nil
  end
  
  
  # Returns a string in the form "id: title, by author”.
  def to_s()
    "#{@id}: #{@title}, by #{@author}"
  end
  
end


# Represents a member of the library.
class Member
  
  @name
  @books
  @library
  
  
  # The constructor sets the properties and with no books checked out.
  # Params:
  # +name+:: the member's name.
  # +library+:: the library to which the member belongs.
 def initialize(name, library)
    
    @name = name
    @books = Array.new
    @library = library
    
  end
  
  
  # Returns this member's name.
  def get_name()
    @name
  end

  
  # Adds this Book object to the set of books checked out by this member (limit of 3 books).
  # Params:
  # +book+:: the book being checked out.
  def check_out(book)
    if @books.length < 3
      @books.push(book)
    end
  end

  
  # Removes this Book object from the set of books checked out by this member. 
  # Params:
  # +book+:: the book being returned.
  def give_back(book)
    @books.delete(book)
  end
  
  
  # Returns the Book objects checked out to this member.
  def get_books()
    @books
  end
  
  
  # Tells this member that he/she has overdue books, formatting a given message.
  # Params:
  # +notice+:: the overdue notice to deliver.
  def send_overdue_notice(notice)
    "#{@name}#{notice}"
  end

end


# Represents a Library where a number of methods are called by the "librarian" (the user),
# not by members, in order to carry out the work. 
class Library

  @calendar
  @members
  @is_open
  @current_member
  @books
  
  
  # The constructor sets the properties, library to closed and populates a set of available books.
  def initialize()

    @calendar = Calendar.new
    @members = Hash.new
    @is_open = false
    @current_member = nil
    @books = create_books()
    
  end
  
  
  # Get the current member.
  def get_current_member()
    @current_member
  end

  
  # Get the available books.
  def get_books()
    @books
  end

  
  # Create an array of library books from from a file named collection.txt. 
  # Each line is a tuple (title,author) and given a unique id number (starting from 1, not 0). 
  # Allows multiple copies of a book but each having a unique id.
  def create_books()
    
    id = 1
    books = Array.new
    
    # Iterate over every file line and create a book for each tuple.
    file = File.new("collection.txt", "r")
    while (line = file.gets)

      # Remove line returns \n
      line.delete!("\n")
      # remove first & last bracket, remove the line return and then split tuple on comma.
      tuple = line[1..-2].split(',')
      title = tuple.first
      author = tuple.last
      
      # Instantiate books and keep record in array
      books.push(Book.new(id, title, author))

      # Increment next book ID
      id = id + 1
           
    end

    file.close   
    
    books
   
  end
  
  
  # Opens the library for business which returns the string "Today is day n.". 
  # If the library is already open, raises an Exception with the message
  # "The library is already open!". Otherwise, starts the day by advancing 
  # the Calendar, and setting the flag to indicate that the library is open.
  def open()
    
    if @is_open
      raise Exception, "The library is already open!"
    else
      @calendar.advance
      @is_open = true
      "Today is day #{@calendar.get_date}."
    end
  
  end
  
  
  # Returns a formatted, multi-line string, listing the names of members who 
  # have overdue books. For each such member, listing the books that are overdue.
  # Or returns the string "No books are overdue.".  
  def find_all_overdue_books()
    
    result = Hash.new
        
    # Select all matches 
    @members.each do |key, member|  
      unless member.get_books.all? { |book| book.get_due_date >= @calendar.get_date }
        result[member.get_name] = member.get_books.select { |book| book.get_due_date < @calendar.get_date }
      end
    end

    # If no matches found
    if result.empty?
      "No books are overdue."
    else

      # Otherwise, build a multi-line string from each array element.
      multiline_result = ""
      result.each do |name, book|
          multiline_result += "#{name}'s overdue books:\n"
          book.each { |book|  multiline_result += "----> #{book.get_title}\n" }
      end      
      
      return multiline_result 
      
    end
  end
  
  
  # Issues a library card to the person with this name. 
  # Returns either "Library card issued to name_of_member." or "name_of_member already has a library card.".
  # Possible Exception: #"The library is not open.".
  # Params:
  # +name_of_member+:: the member's name to whom the card is being issued to.
  def issue_card(name_of_member)
      
    if @is_open == false
        raise Exception, "The library is not open."
      elsif @members.key?(name_of_member)
        "#{name_of_member} already has a library card."
      else
        @members[name_of_member] = Member.new(name_of_member, self) 
        "Library card issued to #{name_of_member}."
      end
  
  end
  
  
  # Specify which member is about to be served by saving the Member object. 
  # Returns either "Now serving name_of_member." or "name_of_member does not have a library card.".
  # Possible Exception: "The library is not open."  
  # Params:
  # +name_of_member+:: the member that is about to be served.
  def serve(name_of_member)
    
    if @is_open == false
      raise Exception, "The library is not open."
    elsif @members.has_key?(name_of_member)
      @current_member = @members[name_of_member]
      "Now serving #{name_of_member}."
    else
      "#{name_of_member} does not have a library card."
    end
    
  end
  
  
  # Find the served member's checked out overdue books and return a multi-line string, 
  # each line containing one book (as returned by the book's to_s method). 
  # If the member has no overdue books, the string "None" is printed.
  # May throw an Exception with an appropriate message:
  # * "The library is not open."
  # * "No member is currently being served."
  def find_overdue_books()
    
    if @is_open == false 
      raise Exception, "The library is not open."
    elsif @current_member. == nil
      raise Exception, "No member is currently being served."    
    end
      
    # Select all matches 
    result = @current_member.get_books.select { |book| book.get_due_date < @calendar.get_date }
    
    # If no books are overdue
    if result.empty?
      "None"
    else
    # Collect overdue books and build a multi-line string, one result per line
        multiline_result = ""
        result.each{ |book| multiline_result += book.to_s + "\n" }
        return multiline_result
    end    
  
  end
  
  
  # Checkin a book checked out to the current member and return it to the 
  # collection of available books. It also removes the book from the member's checked out books.
  # Takes the book_id as a parameter. Returns "name_of_member has returned n books." if successful.
  # May throw an Exception with an appropriate message:
  # * "The library is not open."
  # * "No member is currently being served."
  # * "The member does not have book id.”
  # Params:
  # +*book_numbers:: the book_ids for the books being checked in.
  def check_in(*book_numbers)
    
    if @is_open == false 
      raise Exception, "The library is not open."
    elsif @current_member. == nil
      raise Exception, "No member is currently being served."    
    end
    
    # Loop through the given book_number params.
    book_numbers.each do |book_id|
      
      # If no book_id match is found, raise exception.
      unless get_current_member.get_books.any? { |book| book.get_id == book_id }
        raise Exception, "The member does not have book id."
      end
      
      # Otherwise, get the book's index to access book.
      index = get_current_member.get_books.index{ |book| book.get_id == book_id}
      
      # Checkin the book.
      get_current_member.get_books[index].check_in()
      
      # Add the book to the avaiable books collection.
      @books.push(get_current_member.get_books[index])
      
      # Remove the book from the member's checked out books.
      get_current_member.get_books.delete_at(index)
    
    end
    
    "#{@current_member.get_name} has returned #{book_numbers.length} books."
    
  end


  # Search for books whose title or author (or both) contains this string (case-insensitive)
  # which are currently available (not checked out). Search string must be at least 4 chars
  # and the results include one copy of a book where multiple instances are found.
  # Returns one of following: 
  # * "No books found."
  # * "Search string must contain at least four characters."
  # * A multi-line string, each line containing one book (as returned by the book's to_s method).
  # Params:
  # +string+:: the string to search for.
  def search(string)
    
    if string.length < 4
      "Search string must contain at least four characters."
    elsif @books.empty?
      "No books found."
    else

      # Select all matches (case insensitive)
      result = @books.select { |v| v.get_title.downcase.include?(string.downcase) || v.get_author.downcase.include?(string.downcase) }
      
      if result.empty?
        "No books found." 
      else
        # Remove duplicate copies of the same book.
        result = result.uniq { |book| book.get_title + book.get_author}
        
        # Build a multi-line string, one result per line.
        multiline_result = ""
        result.each{ |book| multiline_result += book.to_s + "\n" }
        return multiline_result
      end
      
    end
  end
  
  
  # Check out books to the member currently being served from a book's ID as a param.
  # A book will then be checked out and removed from library's available books. 
  # Returns "n books have been checked out to name_of_member." if successful.
  # May throw an Exception with an appropriate message:
  # * "The library is not open."
  # * "No member is currently being served."
  # * "The library does not have book id."
  # * "Member cannot checkout more than 3 books." 
  # Params:
  # +*book_ids+:: the book_ids of the books being checked out.
  def check_out(*book_ids) 

    if @is_open == false 
      raise Exception, "The library is not open."
    elsif @current_member. == nil
      raise Exception, "No member is currently being served."    
    elsif (@current_member.get_books().length + book_ids.length) > 3
       raise Exception, "Member cannot checkout more than 3 books." 
    end
    
    # Loop through each book to checkout.
    book_ids.each do |book_id|
      
      # Raise exception if book_id not found in available books.
      unless @books.any? { |book| book.get_id == book_id }
        raise Exception, "The library does not have book id."
      end
      
      # Get the book's index to access book.      
      index = @books.index{ |book| book.get_id == book_id}
      
      # Checkout book and give it a due date of +7 days from today.
      @books[index].check_out(@calendar.get_date + 7)
      
      # Add book to the member's checked out books.
      @current_member.check_out(@books[index])
      
      # Remove book from the libraries available books.
      @books.delete_at(index)
    
    end
    
    "#{book_ids.length} books have been checked out to #{@current_member.get_name}."
    
  end
  
  
  # Renew the specified book_ids held by the member being served by adding 7 extra days 
  # to the due date and returns "n books have been renewed for name_of_member.".
  # May throw an Exception with an appropriate message:
  # * "The library is not open."
  # * "No member is currently being served."
  # * "The member does not have book id."  
  # Params:
  # +*book_ids+:: the book_ids of the book's being renewed.
  def renew(*book_ids)
    
    if @is_open == false 
      raise Exception, "The library is not open."
    elsif @current_member. == nil
      raise Exception, "No member is currently being served."    
    end
    
    # Loop through each book_id to be renewed.
    book_ids.each do |book_id|
      
      # Check that the book_id is held by the customer.
      unless @current_member.get_books.any? { |book| book.get_id == book_id }
        raise Exception, "The member does not have book id."
      end
      
      # If book is held, renew it by adding 7 days from today's date.
      index = @current_member.get_books.index{ |book| book.get_id == book_id}
      @current_member.get_books[index].check_out(@calendar.get_date + 7)
    
    end
    
    "#{book_ids.length} books have been renewed for #{@current_member.get_name}."
    
  end

  
  # Shut down the library's operations until opened again (no operations can be used - except quit()).
  # Returns the string "Good night.", if successful, or, if not open, throws an Exception "The library is not open."  
  def close()
    if @is_open == false
      raise Exception, "The library is not open."
    else
      @is_open = false
      "Good night."
    end
  end
  
  
  # Stop the library and return the string "The library is now closed for renovations.".  
  def quit()
    "The library is now closed for renovations."
  end

end