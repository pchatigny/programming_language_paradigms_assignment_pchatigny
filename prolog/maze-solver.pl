% Programming Paradigms 2014-15 Assignment
% Logic Programming with Prolog
%
% Author::    Patrick Chatigny
% Date::      21 February 2015
%
% The predicate solve(From, To, Path) solves a maze giving the shortest
% path avaiable and prints the resulting maze. From is the starting
% position, To is the target position and Path is the path between the
% two as a list if positions - positions are two element lists.
%
% The maze must be defined with a mazeSize() fact and may contain
% barrier() facts.


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         %
%	SOLVE MAZE        %
%                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Solve and find the shortest path.
solve(From, To, Path) :- shortest_path(From, To, Path).


% Solve recursively.
% Base case if To and From are the same.
solve(T, T, P, P).
% Recursive case - Get positions as X & Y integers, and accumulate
% the current path (CP)
solve([FX, FY|_], [TX, TY|_], CP, P) :-
	move(FX, FY, X, Y),
	\+ member([X,Y], CP),
	add_pos([X, Y],CP,Current_Path),
	solve([X, Y], [TX, TY], Current_Path, P).


% Add element to list keeping the path's order.
% Give element if list is empty.
add_pos(X,[ ],[X]).
% Otherwise add position to tail.
add_pos(X,[H|T],[H|Z]) :- add_pos(X,T,Z).


% Check if position is in bounds of the maze and that its not a barrier
bounds(X,Y) :-
	mazeSize(Z,W),
	X > 0 , Y > 0,
	X =< Z, Y =< W,
	\+ barrier(X,Y).


% Move to adjacent position (horizontal or vertical but not diagonally).
move(FX, FY, TX, TY) :-
	try_move(FX, FY, TX, TY),
	bounds(TX, TY).


% Try to move up, right, down and left
try_move(FX, FY, FX, Y) :- Y is FY + 1.
try_move(FX, FY, X, FY) :- X is FX + 1.
try_move(FX, FY, FX, Y) :- Y is FY - 1.
try_move(FX, FY, X, FY) :- X is FX - 1.


% Get shortest path from available paths using the aggregate predicate.
% Inspired from:
% http://www.swi-prolog.org/pldoc/doc_for?object=section(2,'A.1',swi('/doc/Manual/aggregate.html'))
shortest_path(From, To, Smallest_Path) :-
   aggregate(min(Length, Path),
     (
	 solve(From, To, [From], Path),
	 length(Path, Length)),
	 min(Length, Smallest_Path)
     ),
   write_sol(Smallest_Path).


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         %
%	PRINT MAZE        %
%                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_sol(Solution) :- mazeSize(Rows, Cols), write_header(Cols), write_lines(Rows, Cols, Solution).


% Write	the column number followed by a seperator
write_num(Num) :-
	mazeSize(Max, _),
	CurrentRow is Max + 1 - Num,
	write(CurrentRow),
	write('|').


% Write the header numbers for each column
write_header(-1) :- nl, mazeSize(_, Cols), write_divider(Cols).
write_header(Cols) :-
	mazeSize(_, Max),
	CurrentCol is Max + 0 - Cols,
	write(CurrentCol),
	Cols1 is Cols-1,
	write_header(Cols1).


% Write the a header divider
write_divider(-1) :- nl.
write_divider(Cols) :-
	write('-'),
	Cols1 is Cols-1,
	write_divider(Cols1).


% Write a line followed by a newline
write_line(0, _, _) :- nl.
write_line(Cols, Row, S) :-
	Cols > 0,
	mazeSize(Rmax, Cmax),
	CurrentCol is Cmax + 1 - Cols,
	CurrentRow is Rmax + 1 - Row,
	write_cell(CurrentRow, CurrentCol, S),
	Cols1 is Cols-1,
	write_line(Cols1, Row, S).


% Write each cell as a Barrier, a Path, or empty maze cell
% Write each barrier cell
write_cell(Row, Cols, S) :-
	barrier(Row, Cols),
	\+ member([Row, Cols], S),
	write('+').

% Write each path cell
write_cell(Row, Cols, S) :-
	member([Row, Cols], S),
	write('.').

% Write each empty cell
write_cell(Row, Cols, S) :-
	\+ barrier(Row, Cols),
	\+ barrier(Row, Cols),
	\+ member([Row, Cols], S),
	write(' ').


% Write each line
write_lines(0, _, _).
write_lines(Rows, Cols, S) :-
	Rows > 0,
	Rows1 is Rows-1,
	write_num(Rows),
	write_line(Cols, Rows, S),
	write_lines(Rows1, Cols, S).

