#lang racket
;;; Programming Paradigms 2014-15 Assignment
;;; Functional Programming with Racket
;;;
;;; Author::    Patrick Chatigny
;;; Date::      1 March 2015
;;; 
;;; This program implements a Soduku puzzle solver as required by the assignment. 



;; Make functions available to the test file, bypassing module contraints.
(provide (all-defined-out))
(require racket/trace)



;; Solve the Soduku puzzle given as a list of list. 
;;
;; matrix - A Sudoku puzzle given as a list of lists, with each sub-list representing one row of the puzzle (0 represents a blank space).
;; 
;; Returns the solved Sudoku as a list of lists where 0 have been with approximate numbers - any unsoved locations will be a set of posible integers.
(define (solve matrix)
  
    (solveNtimes 10 (parseSquares (parseCol (parseRow (transform matrix)))))
  
  )



;; Solve the Soduku puzzle n times.
;;
;; matrix - The Soduku matrix as list of sets.
;; n - The number of recursive calls to solveNTimes
;; 
;; Returns a solved Soduku puzzle.
(define (solveNtimes n matrix)
  (when (> n 0)
    (solveNtimes (- n 1) (parseSquares (parseCol (parseRow matrix))))
    matrix
    ))



;; Transforms a Soduku puzzle in the form of list of lists and replaces each integer with a set of integers,
;; replacing 0 with a set of integers {1 2 3 4 5 6 7 8 9} and each non 0 number is replaced by a set containing only that number.
;;
;; matrix - A Sudoku puzzle given as a list of lists, with each sub-list representing one row of the puzzle (0 represents a blank space).
;; 
;; Returns a list of sets of integers where the set is a "possible" number for this location.

(define (transform matrix)
  (for/list ([row matrix])
    (map populate row)))



;; Populate a cell with a zero with a set of all possible options (1 to 9) or with a set containing only that number.
;;
;; x - The element in the list representing a Soduku cell.
;; 
;; Returns a set containing the integers possible for this cell.
(define (populate x)
  (if (= x 0)
      (set 1 2 3 4 5 6 7 8 9)
      (set x)
  ))



;; Parse row -  For each row, remove singletons from possible answer.
;;
;; matrix - The Soduku matrix as list of sets.
;; 
;; Returns a matrix where each singleton found in a row is removed from the "unsolved" sets within that row.
(define (parseRow matrix)
  (for/list ([row matrix])

    ;; Get singlton values from row
    (define singletons (filter (lambda (cell) (= (set-count cell) 1)) row))
    
    ;; Now remove them from the rows  
    (map (lambda (cell) (parseSingle cell singletons)) row)
    
    ))



;; Parse columns - For each column, remove singletons from possible answer.
;;
;; matrix - The Soduku matrix as list of sets.
;; 
;; Returns a matrix where each singleton found in a column is removed from the "unsolved" sets within that column.
(define (parseCol matrix)
  (define i -1)
  (define result 
    (for/list ([row matrix])
      (set! i (+ i 1))
  
      ;; Get column
      (define col (map (lambda (row) (list-ref row i)) matrix))
    
      ;; Get singletons in column
      (define singletons (filter (lambda (cell) (= (set-count cell) 1)) col))

      ;; Now remove singletons from the column cells  
      (map (lambda (cell) (parseSingle cell singletons)) col)
    ))
  
  ;; Re-form the matrix
  (define j -1)
  (for/list ([row result])
    (set! j (+ j 1))
    (map (lambda (row) (list-ref row j)) result))
  )


;; Parse squares - Wrapper functiont that deconstructs matrix into 3x3 squares, remove singletons then recontructs the matrix.
;;
;; matrix - The Soduku matrix as list of sets.
;; 
;; Returns a matrix where each singleton found in a 3x3 square is removed from the "unsolved" sets within that 3x3 square.
(define (parseSquares matrix)
  
  ;; Divide the matrix into 3x3 squares
  (define squares (getSquares matrix))
  
  ;; Remove singletons and reconstruct the matrix
  (reconstructMatrix (parseSquare squares))
  )



;; Parse square - Parse 3x3 square in order to remove singletons.
;;
;; squares - A 3x3 square
;; 
;; Returns a list of list representing the Soduku's nine 3x3 squares where each singleton found in a 3x3 square is removed from the "unsolved" sets within that 3x3 square.
(define (parseSquare squares)
  (for/list ([square squares])

    ;; Get singlton values from square
    (define singletons (filter (lambda (cell) (= (set-count cell) 1)) (flatten square)))
    
    ;; Now remove them from the square  
    (removeSingletonsSquare square singletons)
    ))
  



;; Remove singletons from square - For each square, remove singletons from possible answer.
;;
;; square - The list containing the 3x3 square
;; singletons - the singletons found in the 3x3 square
;; 
;; Returns a list of list representing the Soduku's nine 3x3 squares where each singleton found in a 3x3 square is removed from the "unsolved" sets within that 3x3 square.
(define (removeSingletonsSquare square singletons)
  (for/list ([row square])
    
    ;; Now remove them from the rows  
    (map (lambda (cell) (parseSingle cell singletons)) row)
    
    ))



;; Reconstruct the matrix from a squares matrix.
;;
;; squares - A list of list representing the Soduku's nine 3x3 squares.
;; 
;; Returns the recunstructed Soduku matrix.
(define (reconstructMatrix squares)

  ;; Re-contruct row one
  (define row1 (flatten (map (lambda (row) (list-ref row 0)) (take squares 3))))
    
  ;; Re-contruct row two
  (define row2 (flatten (map (lambda (row) (list-ref row 1)) (take squares 3))))

  ;; Re-contruct row three
  (define row3 (flatten (map (lambda (row) (list-ref row 2)) (take squares 3))))

  ;; Re-contruct row four
  (define row4 (flatten (map (lambda (row) (list-ref row 0)) (drop (take squares 6) 3))))

  ;; Re-contruct row five
  (define row5 (flatten (map (lambda (row) (list-ref row 1)) (drop (take squares 6) 3))))

  ;; Re-contruct row six
  (define row6 (flatten (map (lambda (row) (list-ref row 2)) (drop (take squares 6) 3))))
 
  ;; Re-contruct row seven
  (define row7 (flatten (map (lambda (row) (list-ref row 0)) (drop (take squares 9) 6))))
  
  ;; Re-contruct row eight
  (define row8 (flatten (map (lambda (row) (list-ref row 1)) (drop (take squares 9) 6))))

  ;; Re-contruct row nine
  (define row9 (flatten (map (lambda (row) (list-ref row 2)) (drop (take squares 9) 6))))
  
  (list row1 row2 row3 row4 row5 row6 row7 row8 row9)  
  
  )



;; Get squares - Divide Soduku into nine 3x3 squares and get it as a list of squares.
;;
;; matrix - The Soduku matrix as list of sets.
;; 
;; Returns a list of list representing the Soduku's nine 3x3 squares.
(define (getSquares matrix)

  ;; Get square one
  (define square1 (map (lambda (cell) (take cell 3)) (take matrix 3)))
  
  ;; Get square two
  (define square2 (map (lambda (cell) (drop (take cell 6) 3)) (take matrix 3)))
  
  ;; Get square three
  (define square3 (map (lambda (cell) (drop (take cell 9) 6)) (take matrix 3)))
  
  ;; Get square four
  (define square4 (map (lambda (cell) (take cell 3)) (drop (take matrix 6) 3)))

  ;; Get square five
  (define square5 (map (lambda (cell) (drop (take cell 6) 3)) (drop (take matrix 6) 3)))  
  
  ;; Get square six
  (define square6 (map (lambda (cell) (drop (take cell 9) 6)) (drop (take matrix 6) 3)))
  
  ;; Get square seven
  (define square7 (map (lambda (cell) (take cell 3)) (drop (take matrix 9) 6)))

  ;; Get square eight
  (define square8 (map (lambda (cell) (drop (take cell 6) 3)) (drop (take matrix 9) 6)))  
  
  ;; Get square nine
  (define square9 (map (lambda (cell) (drop (take cell 9) 6)) (drop (take matrix 9) 6)))
  
  (list square1 square2 square3 square4 square5 square6 square7 square8 square9)
  )



;; Parse Cell - Remove singletons from cell.
;;
;; cell - A set containing the possible numbers for this Soduku cell.
;; 
;; Returns the set with the singleton removed.
(define (parseSingle cell singletons)
  (for/fold ([r cell])
            ([s singletons])
    (if (> (set-count r) 1)
        (set-remove r (set-first s)) ; Get the integer from the set and remove from cell (possible values).
        r))
  )

